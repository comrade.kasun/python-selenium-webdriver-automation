from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options

import time
count = 0
while count < 57:
    print(count)
    options = Options()
    options.add_argument("user-data-dir=selenium")
    driver = webdriver.Chrome(options=options)
    driver.get(
        "https://fragnebenan.com/admin/places/address/status:PENDING?page=2")

    # find all dropdown element
    list_drop_down = driver.find_elements_by_class_name("address-status")

    # select confirmed value for each dropdown
    for drop_down_element in list_drop_down:
        select = Select(drop_down_element)
        select.select_by_value('1')

    time.sleep(1)
    driver.close()
    count = count+1
